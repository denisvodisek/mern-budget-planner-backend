const express = require("express");
const router = express.Router();
const Category = require("../models/category");
const categoryMiddleware = require("../middlewares/getCategory");

// Get all category
router.get("/", async (req, res) => {
  try {
    const categories = await Category.find();
    res.json(categories);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Get one category
router.get("/:id", categoryMiddleware.getCategory, async (req, res) => {
  try {
    res.json(res.category);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one category
router.post("/", async (req, res) => {
  const category = new Category({
    name: req.body.name,
    theme: req.body.theme
  });

  try {
    const newCategory = await category.save();
    res.status(201).json(newCategory);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Update one category
router.patch("/:id", categoryMiddleware.getCategory, async (req, res) => {
  if (req.body.name != null) {
    res.category.name = req.body.name;
  }
  if (req.body.theme != null) {
    res.category.theme = req.body.theme;
  }
  try {
    const updatedCategory = await res.category.save();
    res.json(updatedCategory);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one category
router.delete("/:id", categoryMiddleware.getCategory, async (req, res) => {
  try {
    await res.category.remove();
    res.json({ message: "Transaction deleted!" });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;
