const express = require("express");
const router = express.Router();
const Transaction = require("../models/transaction");
const transactionMiddleware = require("../middlewares/getTransaction");

// Get all transactions
router.get("/", async (req, res) => {
  try {
    const transactions = await Transaction.find();
    res.json(transactions);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Get one transaction
router.get("/:id", transactionMiddleware.getTransaction, (req, res) => {
  try {
    res.json(res.category);
    res.send("Hello World");
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

// Create one transaction
router.post("/", async (req, res) => {
  const transaction = new Transaction({
    amount: req.body.amount,
    category: req.body.category,
    description: req.body.description
  });

  try {
    const newTransaction = await transaction.save();
    res.status(201).json(newTransaction);
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
});

// Update one transaction
router.patch("/:id", transactionMiddleware.getTransaction, async (req, res) => {
  if (req.body.name != null) {
    res.transaction.amount = req.body.amount;
  }
  if (req.body.category != null) {
    res.transaction.category = req.body.category;
  }
  if (req.body.description != null) {
    res.transaction.description = req.body.description;
  }
  try {
    const updatedTransaction = await res.transaction.save();
    res.json(updatedTransaction);
  } catch {
    res.status(400).json({ message: err.message });
  }
});

// Delete one transaction
router.delete(
  "/:id",
  transactionMiddleware.getTransaction,
  async (req, res) => {
    try {
      await res.transaction.remove();
      res.json({ message: "Transaction deleted!" });
    } catch (err) {
      res.status(500).json({ message: err.message });
    }
  }
);

module.exports = router;
