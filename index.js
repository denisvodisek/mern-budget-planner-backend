const express = require("express");
const app = express();
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const transactionsRouter = require("./routes/transactions");
const categoryRouter = require("./routes/category");

//accepting json
app.use(express.json());

mongoose.connect("mongodb://127.0.0.1:27017/budget-planner", {
  useNewUrlParser: true
});
const db = mongoose.connection;
db.on("error", error => console.error(error));
db.once("open", () => console.log("connected to database"));

app.use("/transactions", transactionsRouter);
app.use("/category", categoryRouter);

app.listen(3001, () => console.log("server started"));
