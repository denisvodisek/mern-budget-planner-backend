const Transaction = require("../models/transaction");

exports.getTransaction = async (req, res, next) => {
  try {
    transaction = await Transaction.findById(req.params.id);
    if (transaction == null) {
      return res.status(404).json({ message: "Cant find transaction" });
    }
  } catch (err) {
    return res.status(500).json({ message: err.message });
  }

  res.transaction = transaction;
  next();
};
