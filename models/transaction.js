const mongoose = require("mongoose");

const transactionSchema = new mongoose.Schema({
  amount: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  datecreated: {
    type: Date,
    required: true,
    default: Date.now
  }
});

module.exports = mongoose.model("Transaction", transactionSchema);
